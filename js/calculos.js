var resultado;

function operacion(){
  var M = parseFloat(document.getElementById('Masa1').value);
  var m = parseFloat(document.getElementById('masa2').value);
  var r = parseFloat(document.getElementById('distancia').value);
  resultado = ((M * m)/(r**2))*(6.674*10**-11) ;
  document.getElementById("resultado").innerHTML = resultado + " N";
}

function valor_gravedad() {
  var g = document.getElementById("planeta").value;
  switch(g){
    case 'm':
      document.getElementById("gravedad").value = 3.7;
      document.body.style.backgroundImage = "url('./imagenes/Mercurio.jpg')";
      break;
    case 'v':
      document.getElementById("gravedad").value = 8.87;
      document.body.style.backgroundImage = "url('./imagenes/venus.jpg')";
      break;
    case 't':
      document.getElementById("gravedad").value = 9.81;
      document.body.style.backgroundImage = "url('./imagenes/Tierra.jpg')";
      break;
    case 'M':
      document.getElementById("gravedad").value = 3.711;
      document.body.style.backgroundImage = "url('./imagenes/marte.jpg')";
      break;
    case 'j':
      document.getElementById("gravedad").value = 24.79;
      document.body.style.backgroundImage = "url('./imagenes/jupiter.jpg')";
      break;
    case 's':
      document.getElementById("gravedad").value = 10.44;
      document.body.style.backgroundImage = "url('./imagenes/saturno.jpg')";
      break;
    case 'u':
      document.getElementById("gravedad").value = 8.87;
      document.body.style.backgroundImage = "url('./imagenes/urano.jpg')";
      break;
    case 'n':
      document.getElementById("gravedad").value = 11.15;
      document.body.style.backgroundImage = "url('./imagenes/neptuno.jpg')";
      break;
    case '0':
      document.getElementById("gravedad").value = 0;
      document.body.style.backgroundImage = 'none';
      break;
  }
}

function peso_bien() {
  var masa = document.getElementById("Kg").value
  //window.alert(masa);
  if(isNaN(masa)){
    window.alert("numero!")
  }
  else {
    peso(masa)
  }
}

function peso(masa) {
  var KG = parseFloat(document.getElementById("Kg").value);
  var ag = parseFloat(document.getElementById("gravedad").value);
  result = (KG * ag);
  round = result.toFixed(2);
  document.getElementById('result').innerHTML = round + " N";
}
